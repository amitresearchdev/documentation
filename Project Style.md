### change style of action bar color white for whole project 
	
	<!-- Override method -->
	Day Mode
    <style name="ThemeOverlay.AppCompat.Dark.ActionBar" parent="ThemeOverlay.AppCompat.Light"/>
	
	Nignt Mode
	<style name="ThemeOverlay.AppCompat.Dark.ActionBar" parent="ThemeOverlay.AppCompat.Dark"/>
	
	<!-- Defined method -->
	Day Mode
    <style name="AppTheme.AppBarOverlay" parent="ThemeOverlay.AppCompat.Light"/>
    <style name="AppTheme.AppBarOverlay.Dark" parent="ThemeOverlay.AppCompat.Dark"/>
    <style name="AppTheme.PopupOverlay" parent="ThemeOverlay.AppCompat.Light"/>
