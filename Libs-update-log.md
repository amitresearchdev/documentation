### Library Update Sequence
	Helper  -> AdsSdkter    -> Config -> pdf-viewer  
            -> appsfeature            -> mcq-sdk 
                                      -> ytPlayer 
                                      -> mcq-paid-sdk
                                      -> live-exam
                                   


# All Updates

### 11-01-23: Update compileSdk version 33.
``` 
        helper_version = '2.0-alpha02'
        adssdkmaster_version = '1.8-alpha02'
        config_version = '3.5-alpha04'
        
		pdf_viewer_version = '4.1-alpha04' 
		ytplayer_version = '3.3-alpha04'
		mcq_sdk_version = '6.1-alpha04'
		mcq_paid_sdk_version = '3.6-alpha04'
		appfeature_version = '2.3-alpha02' 
```

### 09-01-23: Stable version compileSdk 32.
``` 
        helper_version = '1.9'
        adssdkmaster_version = '1.7'
        config_version = '3.4'
        
		pdf_viewer_version = '4.0' 
		ytplayer_version = '3.2'
		mcq_sdk_version = '6.0'
		mcq_paid_sdk_version = '3.5'
		appfeature_version = '2.2' 
```

### 24-11-22: Update compileSdk 32.
``` 
        helper_version = '1.9-alpha01'
        adssdkmaster_version = '1.7-alpha03'
        config_version = '3.4-alpha03'
        
		pdf_viewer_version = '4.0-alpha02' 
		ytplayer_version = '3.2-alpha02'
		mcq_sdk_version = '6.0-alpha02'
		mcq_paid_sdk_version = '3.5-alpha04'
		appfeature_version = '2.2-alpha01' 
```

### 24-11-22: Before update compile sdk version to 22.
``` 
        helper_version = '1.8'
        adssdkmaster_version = '1.6'
        config_version = '3.3'
        
		pdf_viewer_version = '3.9' 
		ytplayer_version = '3.1'
		mcq_sdk_version = '5.17'
		mcq_paid_sdk_version = '3.4'
		appfeature_version = '2.1' 
```

### 15-02-22: Adding API request timeout retry functionality in config lib
```
    classpath 'com.google.gms:google-services:4.3.8'
    classpath "org.jetbrains.kotlin:kotlin-gradle-plugin:1.5.31"
	
    * All dependencies
        appcompat = '1.3.0'
        material = '1.0.0'
        recyclerview = '1.0.0'
        cardview = '1.0.0'
        swiperefreshlayout = '1.0.0'
        core_ktx = '1.6.0'
        ---
        retrofit_version = '2.9.0'
        retrofit_okhttp_version = '4.8.1'
        picasso_version = '2.71828'
        room_version = "2.2.5"
        ---
        fb_dynamic_links = '20.1.0'
        fb_appindexing = '20.0.0'
        fb_crashlytics_version = '18.0.1'
        firebase_analytics = '19.0.0'
        fbs_library_version_analytics = '19.0.0'
        fbs_library_version_config = '21.0.1'
        google_play_core = '1.9.0'
        fbs_library_version_ads = '19.8.0'
        fbs_library_dynamic_links = '20.1.1'
        fbs_auth_library_version = '21.0.1'
        play_services_safetynet_version = '17.0.0'

	*helper_version = '1.7'
        appcompat = '1.3.0'
        material = '1.0.0'
        retrofit_version = '2.9.0'
        room_version = "2.2.5"
        fbs_library_version_config = '21.0.1'

	*adssdkmaster_version = '1.6-beta02'
        appcompat = '1.3.0'
        recyclerview = '1.0.0'
        cardview = '1.0.0'

        fbs_library_version_config = '21.0.1'
        fbs_library_version_ads = '19.8.0'

        helper_version = '1.7'
		
	*config_version = '3.2-gama05'
        appcompat = '1.3.0'
        material = '1.0.0'
        recyclerview = '1.0.0'
        cardview = '1.0.0'

        retrofit_version = '2.9.0'
        retrofit_okhttp_version = '4.8.1'

        fbs_library_version_config = '21.0.1'
        fbs_auth_library_version = '21.0.1'
        firebase_messaging = '23.0.0'
        play_services_safetynet_version = '17.0.0'

        helper_version = '1.7'
        adssdkmaster_version = '1.6-beta02'

	*pdf_viewer_version = '3.8-beta03'
        appcompat = '1.3.0'
        material = '1.0.0'
        recyclerview = '1.0.0'
        cardview = '1.0.0'

        picasso_version = '2.71828'
        room_version = "2.2.5"
        tedpermission = "2.2.3"
        retrofit_version = '2.9.0'
        retrofit_okhttp_version = '4.8.1'

        fbs_library_version_analytics = '19.0.0'
        fbs_library_dynamic_links = '20.1.1'

        helper_version = '1.7'
        adssdkmaster_version = '1.6-beta02'
        config_version = '3.2-gama05'
		
	*ytplayer_version = '3.1-beta02'
        appcompat = '1.3.0'
        material = '1.0.0'
        cardview = '1.0.0'
        swiperefreshlayout = "1.0.0"

        retrofit_version = '2.9.0'

        helper_version = '1.7'
        adssdkmaster_version = '1.6-beta02'
        config_version = '3.2-gama05'
		
	*mcq_sdk_version = '5.16-beta03'
        appcompat = '1.3.0'
        material = '1.0.0'
        recyclerview = '1.0.0'
        cardview = '1.0.0'
        swiperefreshlayout = '1.0.0'

        retrofit_version = '2.9.0'
        retrofit_okhttp_version = '4.8.1'

        fbs_library_version_config = '21.0.1'

        helper_version = '1.7'
        adssdkmaster_version = '1.6-beta02'
        config_version = '3.2-gama05'
	
	*mcq_paid_sdk_version = '3.3-beta03'
        appcompat = '1.3.0'
        material = '1.0.0'
        recyclerview = '1.0.0'
        cardview = '1.0.0'
        swiperefreshlayout = '1.0.0'

        retrofit_version = '2.9.0'
        retrofit_okhttp_version = '4.8.1'

        fb_dynamic_links = '20.1.0'
        fb_appindexing = '20.0.0'
        firebase_analytics = '19.0.0'

        helper_version = '1.7'
        adssdkmaster_version = '1.6-beta02'
        config_version = '3.2-gama05'
        multidex = '2.0.1'
	
	*appfeature_version = '2.1-beta01' 
        appcompat = '1.3.0'

        google_play_core = '1.9.0'
        fbs_library_version_config = '21.0.1'
        fbs_library_version_analytics = '19.0.0'

        helper_version = '1.7'
       
        
```


### 13-09-22: Remove Location code from helper.
``` 
        helper_version = '1.8-alpha21'
        adssdkmaster_version = '1.6-beta11'
        config_version = '3.3-alpha06'
        
		pdf_viewer_version = '3.9-alpha07' 
		ytplayer_version = '3.1-beta06'
		mcq_sdk_version = '5.17-alpha05'
		mcq_paid_sdk_version = '3.4-alpha24'
		appfeature_version = '2.1-beta04' 
```
### 11-05-22: Admob sdk updated in AdsSdk, compileSdk 31
``` 
        helper_version = '1.8-alpha13'
        adssdkmaster_version = '1.6-beta10'
        config_version = '3.2-gama20'
        
		pdf_viewer_version = '3.8-gama05' 
		ytplayer_version = '3.1-beta04'
		mcq_sdk_version = '5.16-gama12'
		mcq_paid_sdk_version = '3.3-beta05'
		appfeature_version = '2.1-beta02' 
```
### 11-03-22: Adding Rank Sort, DBUpdate and ProgressBar bug fixes in helper
``` 
		helper_version = '1.8-alpha05'
        adssdkmaster_version = '1.6-beta02'
        config_version = '3.2-gama05'
        
		pdf_viewer_version = '3.8-beta03' 
		ytplayer_version = '3.1-beta03'
		mcq_sdk_version = '5.16-beta06'
		mcq_paid_sdk_version = '3.3-beta04'
		appfeature_version = '2.1-beta01' 
```

### 15-02-22: Adding API request timeout retry functionality in config lib
``` 
		helper_version = '1.7'
        adssdkmaster_version = '1.6-beta02'
        config_version = '3.2-gama05'
        
		pdf_viewer_version = '3.8-beta03' 
		ytplayer_version = '3.1-beta02'
		mcq_sdk_version = '5.16-beta03'
		mcq_paid_sdk_version = '3.3-beta03'
		appfeature_version = '2.1-beta01' 
```

### 23-12-21: Adding facebook ads in AdsSdk, Fix stats bug
```
		helper_version = '1.6-alpha02'
        adssdkmaster_version = '1.6-alpha10'
        config_version = '3.2-alpha03'
		pdf_viewer_version = '3.8-alpha04' 
		ytplayer_version = '3.1-alpha03'
		
		mcq_sdk_version = '5.16-alpha05'
		mcq_paid_sdk_version = '3.3-alpha03'
		appfeature_version = '2.1-alpha02' 
```

### 25-11-21: Remove hashCode from config-lib
```
		helper_version = '1.5'
        adssdkmaster_version = '1.5'
        config_version = '3.1'
		pdf_viewer_version = '3.7' 
		ytplayer_version = '3.0'
		
		mcq_sdk_version = '5.15'
		mcq_paid_sdk_version = '3.2'
		appfeature_version = '2.0' 
```

### 26-09-21: Merge config and loginmaster sdks
```
		helper_version = '1.4'
        adssdkmaster_version = '1.2'
        config_version = '3.0-alpha12'
		
		pdf_viewer_version = '3.6-alpha09' 
		ytplayer_version = '2.10-alpha07'
		mcq_sdk_version = '5.11-alpha06'
		mcq_paid_sdk_version = '3.1-alpha05'
		
		appfeature_version = '1.9' 
        loginmaster_version = deprecated
```
		
### 25-09-21: Release all version before merge config and loginmaster sdk
```
		helper_version = '1.4'
        adssdkmaster_version = '1.2'
        config_version = '2.4'
        loginmaster_version = '2.3'
		pdf_viewer_version = '3.5' 
		ytplayer_version = '2.9'
		
		mcq_sdk_version = '5.10'
		mcq_paid_sdk_version = '3.0'
		appfeature_version = '1.9' 
```
		
### 23-09-21: Remove helper common resources
```
		helper_version = '1.4-alpha06'
        adssdkmaster_version = '1.2-alpha03'
        config_version = '2.4-rc10'
        loginmaster_version = '2.3-rc19'
		pdf_viewer_version = '3.5-alpha13' 
		ytplayer_version = '2.9-alpha08'
		
		mcq_sdk_version = '5.10-beta15'
		mcq_paid_sdk_version = '3.0-rc08'
		appfeature_version = '1.9' 
```
		
### 09-09-21: Major change in config-sdk
```
		helper_version = '1.4-alpha06'
        adssdkmaster_version = '1.2-alpha02'
        config_version = '2.4-rc09'
        loginmaster_version = '2.3-rc18'
		pdf_viewer_version = '3.5-alpha11' 
		ytplayer_version = '2.9-alpha06'
		
		mcq_sdk_version = '5.10-beta09'
		mcq_paid_sdk_version = '3.0-rc07'
```
		
### 27-08-21: Major change in AdsSdkMaster
```
        helper_version = '1.4-alpha01'
        adssdkmaster_version = '1.2-alpha02'
		config_version = '2.4-rc08'
        loginmaster_version = '2.3-rc18'
		pdf_viewer_version = '3.5-alpha08' 
		ytplayer_version = '2.9-alpha06'
		
		mcq_sdk_version = '5.10-beta06'
		mcq_paid_sdk_version = '3.0-rc07'
```