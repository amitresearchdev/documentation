### Integration libs Important notes ###

```
1. if update mcq-sdk
	*check login, leaderboard and Que bookmark is available in side menu
2. if update pdf-viewer
	* check Pdf bookmark and Pdf Downloads is available in side menu
	* check pdf delete option.
	* Actionbar download switch button click only if downloaded file is available.
	* Add this code in Application class 'PDFFileUtil.initStorageMigrationOnApiLevel29(appApplication);'

3. if update mcq-sdk-paid
	*check login is available in side menu
	
4. if update config-lib
	*check retry functionality integrated successfully or not.
```
